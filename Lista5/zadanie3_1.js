"use strict";

var ReverseIterator = function(items)
{
    this.index = items.length - 1;
    this.items = items;
}

ReverseIterator.prototype = {
    first: function() {
        this.reset();
        return this.next();
    },
    next: function() {
        return this.items[this.index--];
    },
    hasNext: function() {
        return this.index >= 0;
    },
    reset: function() {
        this.index = items.length - 1;
    },
    each: function(callback) {
        for (var item = this.first(); this.hasNext(); item = this.next()) {
            callback(item);
        }
    }
}


var items = ["1", 2, "3", true, false, 54];

var iter = new ReverseIterator(items);

for (var item = iter.first(); iter.hasNext(); item = iter.next()) {
    console.log(item);
}

function logAlert(item) {
    alert(item);
}


iter.each(logAlert);