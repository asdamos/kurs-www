"use strict";

var Sort = function(sortingMethod)
{
    this.sortingMethod = sortingMethod;
}

Sort.prototype = {
    setSortingMethod: function(sortingMethod)
    {
        this.sortingMethod = sortingMethod;
    },

    sort: function(array) {
        return this.sortingMethod.sort(array);
    }
}


var Bubble = function(){
    this.sort= function (array)
    {
        var n = array.length;

        do
        {
            for(var i = 0; i< n-1; i++)
            {
                if(array[i] > array[i+1])
                {
                    var tmp = array[i];
                    array[i]=array[i+1];
                    array[i+1] = tmp;
                }
            }
            n--;
        }
        while(n > 1);

        return array;
    }
}

var Insert = function(){
    this.sort= function (array)
    {
        var n = array.length;
        var value;
        var i;
        var j;

        for (i = 1; i < n; ++i) {
            value = array[i];
            for (j = i - 1; j >= 0 && array[j] > value; --j)
            {
                array[j + 1] = array[j];
            }
            array[j + 1] = value;
        }
        return array;
    }
}

var Def = function () {
    this.sort= function (array)
    {
        array.sort(function(a, b){return a-b});
        return array;
    }
}


function show(array) {
    console.log("Array:");

    for(var i=0; i<array.length; i++)
    {
        console.log(array[i]);
    }

}


var array1 = [50, 123,1, 54];
var array2 = [50, 123,1, 54];
var array3 = [50, 123,1, 54];

var bubble = new Bubble();
var insert = new Insert();
var def = new Def();

var sorting = new Sort(def);

console.log("Default");
show(array1);
sorting.sort(array1);
show(array1);

sorting.setSortingMethod(bubble);
console.log("Bubble");
show(array2);
sorting.sort(array2);
show(array2);


sorting.setSortingMethod(insert);
console.log("Insert");
show(array3);
sorting.sort(array3);
show(array3);