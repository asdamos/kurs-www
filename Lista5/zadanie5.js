"use strict";

window.onload = function() {
    document.getElementById('accountNumber').onchange = validateAccount;
    document.getElementById('pesel').onchange = validatePesel;
    document.getElementById('birdthDate').onchange = validateBirdth;
    document.getElementById('email').onchange = validateEmail;
    document.getElementById('form').onsubmit = validate;
};

function validateAccount() {
    var element = document.getElementById('accountNumber').value;
    if (element == "") {
        alert("Należy uzupełnić numer konta");
        return false;
    }

    return true;
}

function validatePesel() {
    var element = document.getElementById('pesel').value;
    if (element == "") {
        alert("Należy uzupełnić pesel");
        return false;
    }

    return true;
}

function validateBirdth() {
    var element = document.getElementById('birdthDate').value;
    if (element == "") {
        alert("Należy uzupełnić datę urodzenia");
        return false;
    }

    return true;
}

function validateEmail() {
    var element = document.getElementById('email').value;
    if (element == "") {
        alert("Należy uzupełnić adres e-mail");
        return false;
    }

    return true;
}

function validate() {
    var errors = false;

    if(!validateAccount())
        errors = true;

    if(!validatePesel())
        errors = true;

    if(!validateBirdth())
        errors = true;

    if(!validateEmail())
        errors = true;

    if(!errors)
    {
        var pesel = document.getElementById('pesel').value;
        var birdthDate = document.getElementById('birdthDate').value;

        //date: YYYY-MM-DD
        var ok = false;
        if(pesel[0] === birdthDate[2] && pesel[1] === birdthDate[3]) //year
        {
            if(pesel[3] === birdthDate[5] && pesel[4] === birdthDate[6]) //month
            {
                if(pesel[5] === birdthDate[8] && pesel[6] === birdthDate[9]) //day
                {
                    ok = true;
                }
            }
        }

        if(!ok)
        {
            alert("Pesel nie zgadza się z datą urodzenia")
            return false;
        }
    }

    return true;
}