"use strict";

function isPrime(number)
{
    if(number <= 1)
    {
        return false;
    }
    else if(number <= 3)
    {
        return true;
    }
    else if((number % 2 == 0) || (number % 3 == 0))
    {
        return false;
    }
    var i = 5;
    while(i*i < number)
    {
        if((number % i == 0) || (number % (i+2) == 0))
        {
            return false;
        }
        i += 6;
    }
    return true;
}

function* primes() {
    var number = 1;
    while(true)
    {
        number++;
        if(isPrime(number))
        {
            var reset = yield number;
        }
    }
}

var gen = primes();

function loadPrime() {
    document.getElementById('primeNumber').innerHTML = gen.next().value;
}

window.onload = function() {
    document.getElementById('button').onclick = loadPrime;
};