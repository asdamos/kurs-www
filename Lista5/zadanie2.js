"use strict";

function Vehicle() {
    if(this.constructor === Vehicle)
    {
        throw new Error("Obiekt abstrakcyjny")
    }
    this.x = 0;
    this.y = 0;
    this.fuelConsumption = 1;
    this.tankStatus = 10;
}
//gettery i settery

Object.defineProperty(Vehicle.prototype, "currentPosition", {
    get: function() {var Position = {x:this.x, y:this.y}; return Position},
    set: function (value) {
        this.x = value.x;
        this.y = value.y;
    }
});

Object.defineProperty(Vehicle.prototype, "positionX", {
    get: function() {return this.x},
    set: function (value) {
        this.x = value;
    }
});

Object.defineProperty(Vehicle.prototype, "positionY", {
    get: function() {return this.y},
    set: function (value) {
        this.y = value;
    }
});

Object.defineProperty(Vehicle.prototype, "fuelConsumptionFun", {
    get: function() {return this.fuelConsumption},
    set: function (value) {
        this.fuelConsumption = value;
    }
});

Object.defineProperty(Vehicle.prototype, "tankStatusFun", {
    get: function() {return this.tankStatus},
    set: function (value) {
        this.tankStatus = value;
    }
});



Vehicle.prototype.move = function (x, y) {
    var lenght = (this.x - x)*(this.x - x) + (this.y - y)*(this.y - y);
    lenght = Math.sqrt(lenght);

    this.y = y;
    this.x = x;
    this.tankStatus = this.tankStatus - this.fuelConsumption*lenght;
}

Vehicle.prototype.loadFuel = function (fuelAmount) {
    this.tankStatus = this.tankStatus + fuelAmount;
}

function Car() {
    Vehicle.call(this);
}

Car.prototype = Object.create(Vehicle.prototype);
Car.prototype.constructor = Car;

function Truck() {
    Vehicle.call(this);
}

Truck.prototype = Object.create(Vehicle.prototype);
Truck.prototype.constructor = Truck;

var veh = new Car();

function showProperties() {
    document.getElementById("x").innerHTML = veh.positionX;
    document.getElementById("y").innerHTML = veh.positionY;
    document.getElementById("fuelConsumption").innerHTML = veh.fuelConsumptionFun;
    document.getElementById("tankStatus").innerHTML = veh.tankStatusFun;
}

function executeMove() {
    var x = document.getElementById("xInput").value;
    var y = document.getElementById("yInput").value;

    veh.move(parseFloat(x),parseFloat(y));
    showProperties();
}

function executeLoadFuel() {
    var fuelAmount = document.getElementById("fuelAmount").value;

    veh.loadFuel(parseFloat(fuelAmount));
    showProperties();
}
