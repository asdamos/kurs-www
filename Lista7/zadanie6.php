<?php
$lastModified=filemtime(__FILE__);
$etagFile = md5_file(__FILE__);

$ifModifiedSince=(isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);

$etagHeader=(isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

header("ETag: $etagFile");
header("Last-Modified: ".gmdate("D, d M Y H:i:s", $lastModified)." GMT");
header('Cache-Control: public');


if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE'])==$lastModified || $etagHeader == $etagFile)
{
    header("HTTP/1.1 304 Not Modified");
    exit;
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Adam Sawicki</title>
</head>
<body>

Wylosowana liczba: <?php
echo rand($_GET["min"], $_GET["max"]); ?><br/>

<?php
echo "Ostatnio zmodyfikowano: ".date("d.m.Y H:i:s",time());

?>

</body>
</html>
