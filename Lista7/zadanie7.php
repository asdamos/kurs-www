<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Adam Sawicki</title>
</head>
<body>

<?php
$cookie_name = "nazwa";
$cookie_value = "jakas wartosc";

?>

<?php

if(!isset($_COOKIE[$cookie_name]))
{
    echo "Ciasteczko o nazwie '" . $cookie_name . "' nie jest ustawione";
    setcookie($cookie_name, $cookie_value, time() + 10, '/');
}
else
    {
    echo "Ciasteczko " . $cookie_name . " jest ustawione<br>";
    echo "Zawartość ciasteczka to: " . $_COOKIE[$cookie_name];
}
?>


</body>
</html>
