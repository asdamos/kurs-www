///<reference path="../node_modules/@types/jquery/index.d.ts"/>
var Game = (function () {
    function Game() {
        this.gameOver = false;
        this.rowsNumber = 16;
        this.columnsNumber = 16;
        this.minesNumber = 40;
        this.minesLeft = this.minesNumber;
        this.cellsLeft = this.rowsNumber * this.columnsNumber - this.minesLeft;
        this.bombsNumber = [];
        this.isFlagged = [];
        this.isBomb = [];
        this.isShown = [];
        this.gameStarted = false;
        for (var i = 0; i < this.columnsNumber; i++) {
            this.bombsNumber[i] = [];
            this.isFlagged[i] = [];
            this.isBomb[i] = [];
            this.isShown[i] = [];
            for (var j = 0; j < this.rowsNumber; j++) {
                this.bombsNumber[i][j] = 0;
                this.isFlagged[i][j] = false;
                this.isBomb[i][j] = false;
                this.isShown[i][j] = false;
            }
        }
    }
    Game.prototype.createRows = function () {
        for (var i = 0; i < this.rowsNumber; i++) {
            var trElement = document.createElement("tr");
            $("#game").append(trElement);
        }
    };
    Game.prototype.createColumns = function () {
        for (var i = 0; i < this.columnsNumber; i++) {
            var tdElement = document.createElement("td");
            tdElement.className = "facingDown";
            $("tr").append(tdElement);
        }
    };
    Game.prototype.setOnClickFunction = function () {
        $("td").click(function () {
            var rowIndex = $(this).parent().parent().children().index($(this).parent());
            var colIndex = $(this).parent().children().index($(this));
            clicked(colIndex, rowIndex, "left");
        });
        $("td").contextmenu(function (event) {
            event.preventDefault();
            var rowIndex = $(this).parent().parent().children().index($(this).parent());
            var colIndex = $(this).parent().children().index($(this));
            clicked(colIndex, rowIndex, "right");
            return false;
        });
    };
    Game.prototype.initTable = function () {
        this.createRows();
        this.createColumns();
        this.setOnClickFunction();
    };
    Game.prototype.mineOk = function (x, y, mineX, mineY) {
        if (this.isBomb[mineX][mineY]) {
            return false;
        }
        if (mineY == y && mineX == x) {
            return false;
        }
        if (Math.abs(x - mineX) <= 1 && Math.abs(y - mineY) <= 1) {
            return false;
        }
        return true;
    };
    Game.prototype.initMines = function (x, y) {
        for (var i = 0; i < this.minesNumber; i++) {
            var mineX = void 0;
            var mineY = void 0;
            do {
                mineX = Math.floor(Math.random() * this.columnsNumber);
                mineY = Math.floor(Math.random() * this.rowsNumber);
            } while (!this.mineOk(x, y, mineX, mineY));
            this.isBomb[mineX][mineY] = true;
        }
    };
    Game.prototype.addClassToCell = function (x, y, className) {
        $("#game").children("tr:nth-of-type( " + (y + 1).toString() + " )").children("td:nth-of-type( " + (x + 1).toString() + " )").addClass(className);
    };
    Game.prototype.removeClassFromCell = function (x, y, className) {
        $("#game").children("tr:nth-of-type( " + (y + 1).toString() + " )").children("td:nth-of-type( " + (x + 1).toString() + " )").removeClass(className);
    };
    Game.prototype.countAdjacentBombs = function (x, y) {
        var bombsNumber = 0;
        for (var iShift = -1; iShift < 2; iShift++) {
            for (var jShift = -1; jShift < 2; jShift++) {
                var xPos = x + iShift;
                var yPos = y + jShift;
                if (xPos >= 0 && xPos < this.columnsNumber) {
                    if (yPos >= 0 && yPos < this.rowsNumber) {
                        if (this.isBomb[xPos][yPos]) {
                            bombsNumber++;
                        }
                    }
                }
            }
        }
        return bombsNumber;
    };
    Game.prototype.setFieldNumbers = function () {
        for (var i = 0; i < this.columnsNumber; i++) {
            for (var j = 0; j < this.rowsNumber; j++) {
                if (!this.isBomb[i][j]) {
                    this.bombsNumber[i][j] = this.countAdjacentBombs(i, j);
                }
            }
        }
    };
    Game.prototype.showEmptyGroup = function (x, y) {
        for (var iShift = -1; iShift < 2; iShift++) {
            for (var jShift = -1; jShift < 2; jShift++) {
                var xPos = x + iShift;
                var yPos = y + jShift;
                if (xPos >= 0 && xPos < this.columnsNumber) {
                    if (yPos >= 0 && yPos < this.rowsNumber) {
                        if (!this.isBomb[xPos][yPos]) {
                            this.showField(xPos, yPos);
                        }
                    }
                }
            }
        }
    };
    Game.prototype.showField = function (x, y) {
        if (!this.isShown[x][y]) {
            this.cellsLeft--;
            this.isShown[x][y] = true;
            this.removeClassFromCell(x, y, "facingDown");
            switch (this.bombsNumber[x][y]) {
                case 0:
                    this.addClassToCell(x, y, "zero");
                    this.showEmptyGroup(x, y);
                    break;
                case 1:
                    this.addClassToCell(x, y, "one");
                    break;
                case 2:
                    this.addClassToCell(x, y, "two");
                    break;
                case 3:
                    this.addClassToCell(x, y, "three");
                    break;
                case 4:
                    this.addClassToCell(x, y, "four");
                    break;
                case 5:
                    this.addClassToCell(x, y, "five");
                    break;
                case 6:
                    this.addClassToCell(x, y, "six");
                    break;
                case 7:
                    this.addClassToCell(x, y, "seven");
                    break;
                case 8:
                    this.addClassToCell(x, y, "eight");
                    break;
            }
        }
    };
    Game.prototype.gameLost = function (x, y) {
        this.removeClassFromCell(x, y, "facingDown");
        this.addClassToCell(x, y, "boom");
        this.isShown[x][y] = true;
        for (var i = 0; i < this.columnsNumber; i++) {
            for (var j = 0; j < this.rowsNumber; j++) {
                if (this.isBomb[i][j] && !this.isShown[i][j]) {
                    this.removeClassFromCell(i, j, "facingDown");
                    this.addClassToCell(i, j, "bomb");
                }
            }
        }
        clearInterval(timerInterval);
        alert("Przegrano!");
        this.gameOver = true;
    };
    Game.prototype.gameWon = function () {
        clearInterval(timerInterval);
        alert("Gratulacje, wygrano!");
        this.gameOver = true;
    };
    Game.prototype.minesCounterUpdate = function () {
        $("#minesLeftCounter").text(this.minesLeft);
    };
    return Game;
}());
function clicked(x, y, mouseButton) {
    if (!mines.gameOver) {
        if (!mines.gameStarted) {
            if (mouseButton == "left") {
                mines.initMines(x, y);
                mines.gameStarted = true;
                mines.setFieldNumbers();
                mines.showField(x, y);
                timerInterval = setInterval(function () { updateTimer(); }, 1000);
                gameStart = new Date();
            }
        }
        else {
            if (mouseButton == "left") {
                if (!mines.isShown[x][y] && !mines.isFlagged[x][y]) {
                    if (mines.isBomb[x][y]) {
                        mines.gameLost(x, y);
                    }
                    else {
                        mines.showField(x, y);
                        if (mines.cellsLeft == 0) {
                            mines.gameWon();
                        }
                    }
                }
            }
            else {
                if (!mines.isShown[x][y]) {
                    if (mines.isFlagged[x][y]) {
                        mines.isFlagged[x][y] = false;
                        mines.removeClassFromCell(x, y, "flagged");
                        mines.addClassToCell(x, y, "facingDown");
                        mines.minesLeft++;
                        mines.minesCounterUpdate();
                    }
                    else {
                        mines.isFlagged[x][y] = true;
                        mines.removeClassFromCell(x, y, "facingDown");
                        mines.addClassToCell(x, y, "flagged");
                        mines.minesLeft--;
                        mines.minesCounterUpdate();
                    }
                }
            }
        }
    }
}
var mines = new Game();
var timerInterval;
var gameStart;
function updateTimer() {
    var time = new Date();
    var diff = time.getTime() - gameStart.getTime();
    var seconds = diff / 1000;
    seconds = Math.round(seconds);
    var minutes = 0;
    if (seconds >= 60) {
        minutes = seconds / 60;
        minutes = Math.floor(minutes);
        seconds = seconds - minutes * 60;
    }
    $("#timer").text(minutes.toString() + ":" + seconds.toString());
}
window.onload = function () {
    mines.initTable();
    mines.minesCounterUpdate();
};
