﻿///<reference path="../node_modules/@types/jquery/index.d.ts"/>
class Game {
    public bombsNumber: number[][];
    public isFlagged: boolean[][];
    public isBomb: boolean[][];
    public isShown: boolean[][];
    public gameStarted: boolean;

    public rowsNumber: number;
    public columnsNumber: number;
    public minesNumber: number;
    public minesLeft: number;
    public gameOver: boolean;

    public cellsLeft: number;

    constructor()
    {
        this.gameOver = false;

        this.rowsNumber = 16;
        this.columnsNumber = 16;
        this.minesNumber = 40;

        this.minesLeft = this.minesNumber;

        this.cellsLeft = this.rowsNumber * this.columnsNumber - this.minesLeft;

        this.bombsNumber = [];
        this.isFlagged = [];
        this.isBomb = [];
        this.isShown = [];
        this.gameStarted = false;
        for(let i:number = 0; i < this.columnsNumber; i++)
        {
            this.bombsNumber[i] = [];
            this.isFlagged[i] = [];
            this.isBomb[i] = [];
            this.isShown[i] = [];

            for(let j: number = 0; j< this.rowsNumber; j++) {
                this.bombsNumber[i][j] = 0;
                this.isFlagged[i][j] = false;
                this.isBomb[i][j] = false;
                this.isShown[i][j] = false;
            }
        }
    }

    private createRows()
    {
        for(let i: number = 0; i<this.rowsNumber; i++)
        {
            let trElement = document.createElement("tr");
            $("#game").append(trElement);
        }
    }

    private createColumns()
    {
        for(let i: number = 0; i<this.columnsNumber; i++)
        {
            let tdElement = document.createElement("td");
            tdElement.className = "facingDown";
            $("tr").append(tdElement);
        }
    }

    private setOnClickFunction()
    {
        $("td").click(function() {
                let rowIndex = $(this).parent().parent().children().index($(this).parent());
                let colIndex = $(this).parent().children().index($(this));
                clicked(colIndex, rowIndex, "left");
            }
        );

        $("td").contextmenu(function (event) {
            event.preventDefault();
            let rowIndex = $(this).parent().parent().children().index($(this).parent());
            let colIndex = $(this).parent().children().index($(this));
            clicked(colIndex, rowIndex, "right");
            return false;
        });

    }

    initTable()
    {
        this.createRows();
        this.createColumns();
        this.setOnClickFunction();
    }

    private mineOk(x: number, y:number, mineX:number, mineY:number)
    {
        if(this.isBomb[mineX][mineY])
        {
            return false;
        }

        if(mineY == y && mineX == x)
        {
            return false;
        }

        if(Math.abs(x-mineX) <= 1 && Math.abs(y-mineY) <=1)
        {
            return false;
        }


        return true;
    }

    initMines(x:number, y:number)
    {
        for(let i:number = 0; i<this.minesNumber; i++)
        {
            let mineX : number;
            let mineY : number;
            do
            {
                mineX = Math.floor(Math.random()*this.columnsNumber);
                mineY = Math.floor(Math.random()*this.rowsNumber);
            }
            while(!this.mineOk(x,y,mineX,mineY));

            this.isBomb[mineX][mineY] = true;
        }
    }

    addClassToCell(x:number, y:number, className: string)
    {
        $("#game").children("tr:nth-of-type( " + (y + 1).toString() + " )").children("td:nth-of-type( " + (x + 1).toString() + " )").addClass(className);
    }

    removeClassFromCell(x:number, y:number, className: string)
    {
        $("#game").children("tr:nth-of-type( " + (y + 1).toString() + " )").children("td:nth-of-type( " + (x + 1).toString() + " )").removeClass(className);

    }

    private countAdjacentBombs(x:number, y:number)
    {
        let bombsNumber : number = 0;
        for(let iShift:number = -1; iShift<2; iShift++)
        {
            for(let jShift:number = -1; jShift<2; jShift++)
            {
                let xPos = x + iShift;
                let yPos = y + jShift;

                if(xPos >=0 && xPos<this.columnsNumber)
                {
                    if(yPos >=0 && yPos<this.rowsNumber)
                    {
                        if(this.isBomb[xPos][yPos])
                        {
                            bombsNumber++;
                        }
                    }
                }
            }
        }

        return bombsNumber;
    }

    setFieldNumbers()
    {
        for(let i:number = 0; i<this.columnsNumber; i++)
        {
            for(let j:number = 0; j<this.rowsNumber; j++)
            {
                if(!this.isBomb[i][j])
                {
                    this.bombsNumber[i][j] = this.countAdjacentBombs(i, j);
                }
            }
        }
    }

    private showEmptyGroup(x:number, y:number)
    {
        for(let iShift:number = -1; iShift<2; iShift++)
        {
            for(let jShift:number = -1; jShift<2; jShift++)
            {
                let xPos = x + iShift;
                let yPos = y + jShift;

                if(xPos >=0 && xPos<this.columnsNumber)
                {
                    if(yPos >=0 && yPos<this.rowsNumber)
                    {
                        if(!this.isBomb[xPos][yPos])
                        {
                            this.showField(xPos, yPos);
                        }
                    }
                }
            }
        }
    }

    showField(x:number, y:number)
    {
        if(!this.isShown[x][y])
        {
            this.cellsLeft--;
            this.isShown[x][y] = true;
            this.removeClassFromCell(x, y, "facingDown");
            switch(this.bombsNumber[x][y])
            {
                case 0:
                    this.addClassToCell(x, y, "zero");
                    this.showEmptyGroup(x, y);
                    break;
                case 1:
                    this.addClassToCell(x, y, "one");
                    break;
                case 2:
                    this.addClassToCell(x, y, "two");
                    break;
                case 3:
                    this.addClassToCell(x, y, "three");
                    break;
                case 4:
                    this.addClassToCell(x, y, "four");
                    break;
                case 5:
                    this.addClassToCell(x, y, "five");
                    break;
                case 6:
                    this.addClassToCell(x, y, "six");
                    break;
                case 7:
                    this.addClassToCell(x, y, "seven");
                    break;
                case 8:
                    this.addClassToCell(x, y, "eight");
                    break;
            }
        }
    }

    gameLost(x:number, y:number)
    {
        this.removeClassFromCell(x, y, "facingDown");
        this.addClassToCell(x, y, "boom");
        this.isShown[x][y] = true;

        for(let i:number = 0; i<this.columnsNumber; i++)
        {
            for(let j:number = 0; j<this.rowsNumber; j++)
            {
                if(this.isBomb[i][j] && !this.isShown[i][j])
                {
                    this.removeClassFromCell(i, j, "facingDown");
                    this.addClassToCell(i, j, "bomb");
                }
            }
        }

        clearInterval(timerInterval);
        alert("Przegrano!");
        this.gameOver = true;
    }

    gameWon()
    {
        clearInterval(timerInterval);
        alert("Gratulacje, wygrano!");
        this.gameOver = true;
    }


    minesCounterUpdate()
    {
        $("#minesLeftCounter").text(this.minesLeft);
    }

}

function clicked(x:number, y:number, mouseButton : string)
{
    if(!mines.gameOver)
    {
        if(!mines.gameStarted)
        {
            if(mouseButton == "left")
            {
                mines.initMines(x, y);
                mines.gameStarted = true;
                mines.setFieldNumbers();
                mines.showField(x, y);

                timerInterval = setInterval(function () {updateTimer()},  1000);
                gameStart = new Date();
            }
        }
        else
        {
            if(mouseButton == "left")
            {
                if(!mines.isShown[x][y] && !mines.isFlagged[x][y])
                {
                    if(mines.isBomb[x][y])
                    {
                        mines.gameLost(x, y);
                    }
                    else
                    {
                        mines.showField(x, y);

                        if(mines.cellsLeft == 0)
                        {
                            mines.gameWon();
                        }
                    }
                }
            }
            else
            {
                if(!mines.isShown[x][y])
                {
                    if(mines.isFlagged[x][y])
                    {
                        mines.isFlagged[x][y] = false;
                        mines.removeClassFromCell(x, y, "flagged");
                        mines.addClassToCell(x, y, "facingDown");

                        mines.minesLeft++;
                        mines.minesCounterUpdate();
                    }
                    else
                    {
                        mines.isFlagged[x][y] = true;
                        mines.removeClassFromCell(x, y, "facingDown");
                        mines.addClassToCell(x, y, "flagged");

                        mines.minesLeft--;
                        mines.minesCounterUpdate();
                    }
                }
            }
        }
    }
}


let mines = new Game();

let timerInterval;

let gameStart:Date;

function updateTimer()
{
    let time:Date = new Date();

    let diff = time.getTime() - gameStart.getTime( );

    let seconds:number = diff / 1000;

    seconds = Math.round(seconds);

    let minutes:number = 0;
    if(seconds >= 60)
    {
        minutes = seconds / 60;
        minutes = Math.floor(minutes);
        seconds = seconds - minutes*60;
    }

    $("#timer").text(minutes.toString()+":"+seconds.toString());
}

window.onload = function() {

    mines.initTable();
    mines.minesCounterUpdate();

};

